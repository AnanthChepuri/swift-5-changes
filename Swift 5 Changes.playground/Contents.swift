import UIKit

                                                    //  Language Improvements

//  1.  Testing Integer Multiples  [isMultiple(of:)]

let rowNumber = 4

if rowNumber % 2 == 0 {     // Old way
    print("Even")
} else {
    print("Odd")
}

if rowNumber.isMultiple(of: 2) {     // New way
    print("Even")
} else {
    print("Odd")
}

//  2.  Escaping Raw Strings

//  Swift 4.2 uses escape sequences to represent backslashes and quote marks in strings:
let escape = "You use escape sequences for \"quotes\"\\\"backslashes\" in Swift 4.2."
print(escape)

//  Swift 5 adds raw strings. You add # at the beginning and end of the string so you can use backslashes and quote marks without issue.
let raw = #"You can create "raw"\"plain" strings in Swift 5."#
print(raw)

//  3.  Using New Character Properties  (isNumber, isAlphabetic)

let id = "2ID10"
var digits = 0
id.forEach { digits += Int(String($0)) != nil ? 1 : 0 }
print("Id has \(digits) digits.")

var digits2 = 0
id.forEach { digits2 += $0.isNumber ? 1 : 0 }
print("Id has \(digits2) digits.")


//  4.  Removing Subsequences
/*
     Subsequences are now replaced with [Element]
     Now methods like dropLast(), dropLast(_:) will return [Element] now...
 */

extension Sequence {
    func remove(_ s: String) -> [Element] { //[SubSequence] {
    guard let n = Int(s) else {
      return dropLast()
    }
    return dropLast(n)
  }
}

let sequence = [5, 2, 7, 4]
sequence.remove("2") // [5, 2]
sequence.remove("two") // [5, 2, 7]\


                                                //  Dictionary updates

//  1.  compactMapValues(_:)

let students = ["Oana": "10", "Nori": "ten"]
let filterStudents = students.mapValues(Int.init)
  .filter { $0.value != nil }
  .mapValues { $0! }
let reduceStudents = students.reduce(into: [:]) { $0[$1.key] = Int($1.value) }
print(reduceStudents)

//  New way
let mapStudents = students.compactMapValues(Int.init)
print(mapStudents)

//  2.  Renaming DictionaryLiterals ->  Key-value pairs

//  Swift 4.2 uses DictionaryLiteral to declare dictionaries
let pets: DictionaryLiteral = ["dog": "Sclip", "cat": "Peti"]

//  Swift 5 renames DictionaryLiteral to KeyValuePairs
let pets1 : KeyValuePairs = ["dog": "Sclip", "cat": "Peti"]

                                                //  String Interpolation

//  Swift 4.2 implements string interpolation by interpolating segments:
/*
let language = "Swift"
let languageSegment = String(stringInterpolationSegment: language)
 
 stringInterpolationSegment is removed below is the new approach
 
*/

// 1
var interpolation = DefaultStringInterpolation(
  literalCapacity: 7,
  interpolationCount: 1)
// 2
let language = "Swift"
interpolation.appendLiteral(language)
let space = " "
interpolation.appendLiteral(space)
let version = 5
interpolation.appendInterpolation(version)
// 3
let string = String(stringInterpolation: interpolation)

                                                //  Handling Future Enumeration Cases
/*
    Swift 4.2 doesn’t warn you added a new case to Switch is exhaustive
    By @unknown at default case, Swift 5 warns you about newly added cases in switch
 */

// 1
enum Post {
  case tutorial, article, screencast, course, podcast
}

// 2
func readPost(_ post: Post) -> String {
  switch post {
    case .tutorial:
      return "You are reading a tutorial."
    case .article:
      return "You are reading an article."
  @unknown default:
      return "You are watching a video."
  }
}

// 3
let screencast = Post.screencast
readPost(screencast) // "You are watching a video."
let course = Post.course
readPost(course) // "You are watching a video."
let podcast = Post.podcast
readPost(podcast)

                                            //  Adding "Result" to Swift standard library

//  Swift 5 adds Result to the standard library

/*
 
func fetchPosts(url: URL, completion: @escaping (Result<[Post],NetworkError>) -> Void) {
        URLSession.shared.dataTask(with: url) { data, response, error in
        guard let data = data, error == nil else {
            if let error = error as NSError?, error.domain == NSURLErrorDomain {
                    completion(.failure(.domainError))
            }
            return
        }
        
        do {
            let posts = try JSONDecoder().decode([Post].self, from: data)
            completion(.success(posts))
        } catch {
            completion(.failure(.decodingError))
        }
        
    }.resume()
    
}

let url = URL(string: "https://jsonplaceholder.typicode.com/posts")!

fetchPosts(url: url) { result in
    
    switch result {
        case .success(let posts):
            print(posts)
        case .failure:
            print("FAILED")
    }
})
 */
