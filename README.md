The project explains some new features in Swift 5. Topics are...

1. Language Improvements
	* Testing Integer Multiples  [isMultiple(of:)]
    * Escaping Raw Strings
    * Using New Character Properties  (isNumber, isAlphabetic)
    * Removing Subsequences


2. Dictionary updates
	* compactMapValues(_:)
    * Renaming DictionaryLiterals ->  Key-value pairs


3. String Interpolation

4. Handling Future Enumeration Cases

5. Adding "Result" to Swift standard library
